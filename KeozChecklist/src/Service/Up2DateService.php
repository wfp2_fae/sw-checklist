<?php

namespace Keoz\KeozChecklist\Service;

use Doctrine\DBAL\Connection;

class Up2DateService implements CheckServiceInterface
{
    private $dbal;

    public function __construct(Connection $dbal)
    {
        $this->dbal = $dbal;
    }

    public function getCheckResult()
    {
        $plugins = $this->getPlugins();
        $checkResult = [];

        foreach ($plugins as $plugin) {
            $name = $plugin['name'];
            $version = $plugin['version'];
            $upgrade = $plugin['upgrade_version'];

            if ($name == "KeozPlatformCheckList")
            {
                $checkResult[] = [
                    'property' => "keoz-checklist.list.pluginVersion",
                    'value' => $version,
                    'should' => $upgrade ? $upgrade : $version,
                    'status' => $upgrade ? false : true,
                ];
            }
        }

        return $checkResult;
    }

    private function getPlugins()
    {
        $builder = $this->dbal->createQueryBuilder();

        $builder->select([
                'name',
                'version',
                'upgrade_version',
            ])
            ->from('plugin');

        return $builder->execute()->fetchAll();
    }
}

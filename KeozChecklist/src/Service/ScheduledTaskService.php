<?php

namespace Keoz\KeozChecklist\Service;

use Doctrine\DBAL\Connection;

class ScheduledTaskService implements CheckServiceInterface
{
    private $dbal;

    public function __construct(Connection $dbal)
    {
        $this->dbal = $dbal;
    }

    public function getCheckResult()
    {
        $scheduledTasks = $this->getScheduledTasks();

        $data = [];

        $now = date_create();

        foreach ($scheduledTasks as $task) {
            $taskCheck = [];

            $lastExecutionTime = date_create($task['last_execution_time']);
            $nextExecutionTime = date_create($task['next_execution_time']);
            $interval = intval($task['run_interval']);

            $taskCheck['property'] = 'schedTask ['.$task['name'].'] lastExec';

            $shouldTime = $this->getShouldRunTime($lastExecutionTime, $interval, $now);
            $taskCheck['should'] = $shouldTime->format('d.m.Y H:i:s');

            if (!$this->hasEverRun($task)) {
                $status = $this->getStatus($nextExecutionTime, 0, $now);
                $value = 'never';
            } else {
                $status = $this->getStatus($lastExecutionTime, $interval, $now);
                $value = $lastExecutionTime->format('d.m.Y H:i:s');
            }

            $taskCheck['status'] = $status;
            $taskCheck['value'] = $value;

            $data[] = $taskCheck;
        }

        return $data;
    }

    private function getScheduledTasks()
    {
        $builder = $this->dbal->createQueryBuilder();

        $builder->select('*')->from('scheduled_task');

        return $builder->execute()->fetchAll();
    }

    private function hasEverRun($task)
    {
        if (!$task['last_execution_time']) {
            return false;
        }

        return true;
    }

    private function getStatus($lastExecutionTime, $interval, $now)
    {
        $lastExecutionStamp = $lastExecutionTime->getTimestamp();
        $nowStamp = $now->getTimeStamp();

        $difference = $nowStamp - $lastExecutionStamp;

        return !($difference > $interval);
    }

    private function getShouldRunTime($lastExecutionTime, $interval, $now)
    {
        $lastExecutionStamp = $lastExecutionTime->getTimestamp();
        $nowStamp = $now->getTimestamp();

        $shouldRunStamp = $lastExecutionStamp + $interval;

        $shouldTime = date_create()->setTimestamp($shouldRunStamp);

        return $shouldTime;
    }
}

<?php

namespace Keoz\KeozChecklist\Service;

class CacheService implements CheckServiceInterface
{
    private $cacheDirectory;

    public function __construct($cacheDirectory)
    {
        $this->cacheDirectory = $cacheDirectory;
    }

    public function getCheckResult()
    {
        $size = $this->getSize($this->cacheDirectory);
        $sizeResult = $this->fixMeasure($size);

        $size = $sizeResult['size'];
        $measure = $sizeResult['measure'];

        $time = filemtime($this->cacheDirectory);
        $fileChangeDate = date_create("@$time");

        $today = new \DateTime();
        $today = $today->getTimestamp();

        $shouldDate = $today - 86400;
        $shouldDate = date('d.m.Y H:i:s', $shouldDate);

        $d = date('d.m.Y H:i:s', $time);

        return [
            [
                'property' => 'keoz-checklist.list.cacheSize',
                'value' => "$size $measure",
                'should' => '',
                'status' => '',
            ],
            [
                'property' => 'keoz-checklist.list.cacheLastChange',
                'value' => $d,
                'should' => "min. $shouldDate",
                'status' => ($today - $time >= 86400) ? false : true,
            ],
        ];
    }

    private function getSize($path, $size = 0)
    {
        if (!is_dir($path)) {
            $size += filesize($path);
        } else {
            $dir = opendir($path);
            while ($file = readdir($dir)) {
                if (is_file($path.DIRECTORY_SEPARATOR.$file)) {
                    $size += filesize($path.DIRECTORY_SEPARATOR.$file);
                }

                if (is_dir($path.DIRECTORY_SEPARATOR.$file) && $file != '.' && $file != '..') {
                    $size = $this->getSize($path.DIRECTORY_SEPARATOR.$file, $size);
                }
            }
        }

        return $size;
    }

    private function fixMeasure($size)
    {
        $measure = 'Byte';
        if ($size >= 1024) {
            $measure = 'KB';
            $size = $size / 1024;
        }

        if ($size >= 1024) {
            $measure = 'MB';
            $size = $size / 1024;
        }
        if ($size >= 1024) {
            $measure = 'GB';
            $size = $size / 1024;
        }

        $size = sprintf('%01.2f', $size);

        return [
            'size' => $size,
            'measure' => $measure,
        ];
    }

    private function getFileChangeTime($file)
    {
        $time = filemtime($file);

        return date_create("@$time");
    }

    private function getShouldTime($today)
    {
        $shouldDate = $today - 86400;

        return date_create($shouldDate);
    }
}

<?php

namespace Keoz\KeozChecklist\Service;

interface CheckServiceInterface
{
    public function getCheckResult();
}

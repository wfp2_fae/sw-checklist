<?php

namespace Keoz\KeozChecklist\Service;

class PHPService implements CheckServiceInterface
{
    const PHP_RECOMMENDED_VERSION = '7.2.0';

    public function getCheckResult()
    {
        return [
            [
                'property' => 'keoz-checklist.list.phpVersion',
                'value' => phpversion(),
                'should' => self::PHP_RECOMMENDED_VERSION.'phphigher',
                'status' => version_compare(phpversion(), self::PHP_RECOMMENDED_VERSION, '>=')
                    && phpversion() != '7.2.20'
                    && phpversion() != '7.3.7',
            ],
        ];
    }
}

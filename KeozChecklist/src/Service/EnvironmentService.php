<?php

namespace Keoz\KeozChecklist\Service;

use Symfony\Component\HttpKernel\KernelInterface;

class EnvironmentService implements CheckServiceInterface
{
    /**
     * @var string
     */
    private $environment;

    /**
     * EnvironmentService constructor.
     * @param KernelInterface $kernel
     */
    public function __construct(KernelInterface $kernel)
    {
        $this->environment = $kernel->getEnvironment();
    }

    public function getCheckResult(): array
    {
        return [
            [
                'property' => 'keoz-checklist.list.environment',
                'value' => $this->environment,
                'should' => 'prod',
                'status' => $this->environment === 'prod',
            ],
        ];
    }
}

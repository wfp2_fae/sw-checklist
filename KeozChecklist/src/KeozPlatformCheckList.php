<?php

namespace Keoz\KeozChecklist;

use Shopware\Core\Framework\Plugin;

class KeozPlatformCheckList extends Plugin
{
    public function getServicesFilePath(): string
    {
        return '/Resources/config/services.xml';
    }
}

import ApiService from 'src/core/service/api.service';

export default class ChecklistApiService extends ApiService {
    constructor(httpClient, loginService, apiEndpoint = 'keoz-checklist') {
        super(httpClient, loginService, apiEndpoint);
        this.name = 'checklistApiService';
    }

    getResult() {
        return this.httpClient
            .get('keoz/keoz-checklist', {
                params: {},
                headers: this.getBasicHeaders({})
            })
            .then((response) => {
                return ApiService.handleResponse(response);
            });
    }
}

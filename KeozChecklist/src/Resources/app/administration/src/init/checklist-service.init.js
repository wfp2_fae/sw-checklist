import CheckListApiService from '../services/checklist.api.service.js';

Shopware.Application.addServiceProvider('checklistApiService', container => {
    const initContainer = Shopware.Application.getContainer('init');
    const serviceContainer = Shopware.Application.getContainer('service');
    return new CheckListApiService(initContainer.httpClient, serviceContainer.loginService);
});

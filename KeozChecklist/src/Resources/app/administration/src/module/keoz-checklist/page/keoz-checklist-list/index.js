import { Component } from 'src/core/shopware';
import template from './keoz-checklist-list.html.twig';
import './keoz-checklist-list.scss';
import { Application } from 'src/core/shopware';

Component.register('keoz-checklist-list', {
    template,

    inject: {
        checklistApiService: 'checklistApiService'
    },

    data() {
        return {
            result: []
        };
    },

    created() {
        this.loadResult();
    },

    computed: {
        checklistColumns() {
            return this.getCheckColumns();
        },

        apiServiceFactory() {
            return Application.getContainer('factory').apiService;
        }
    },

    methods: {

        getCheckColumns() {
            return [{
                property: "property",
                label: this.$tc('keoz-checklist.columns.property')
            }, {
                property: "value",
                label: this.$tc('keoz-checklist.columns.value')
            }, {
                property: "should",
                label: this.$tc('keoz-checklist.columns.should')
            }, {
                property: "status",
                label: this.$tc('keoz-checklist.columns.status')
            }];
        },

        loadResult() {
            this.checklistApiService.getResult().then((response) => {
                var items = response["data"];
                const me = this;

                items.forEach(function(item){
                    if (item.property.includes("schedTask") && item.property.includes("lastExec")) {
                        item.property = item.property.replace("schedTask", me.$tc("keoz-checklist.list.schedTask"));
                        item.property = item.property.replace("lastExec", me.$tc("keoz-checklist.list.lastExec"));
                    } else {
                        item.property = me.$tc(item.property);
                    }

                    if (item.value == "never") {
                        item.value = me.$tc("keoz-checklist.list.never");
                    }

                    if (item.should.includes("phphigher")) {
                        item.should = item.should.replace("phphigher", me.$tc("keoz-checklist.list.phphigher"));
                    }
                });

                this.result = items;
            });
        }
    }
});

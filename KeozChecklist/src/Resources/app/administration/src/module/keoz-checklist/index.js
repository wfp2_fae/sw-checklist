import { Module } from 'src/core/shopware';
import './page/keoz-checklist-list';
import deDE from "./snippet/de-DE";
import enGB from "./snippet/en-GB";

Module.register('keoz-checklist', {
    type: 'plugin',
    name: 'Checklist',
    title: 'Checklist',
    description: 'Simple check of conditions for production.',
    color: 'ff0000',
    icon: 'small-pencil-paper',

    snippets: {
        'de-DE': deDE,
        'en-GB': enGB
    },

    routes: {
        index: {
            component: "keoz-checklist-list",
            path: 'index'
        }
    },

    settingsItem: {
        group: 'system',
        to: 'keoz.checklist.index',
        icon: 'default-documentation-paper-pencil-signed'
    }
});

<?php

namespace Keoz\KeozChecklist\Controller;

use Shopware\Core\Framework\Context;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Shopware\Core\Framework\Routing\Annotation\RouteScope;

/**
 * @RouteScope(scopes={"api"})
 */
class KeozCheckList extends AbstractController
{
    private $services;

    public function __construct(iterable $services){
        $this->services = $services;
    }

    /**
     * @Route("/api/v{version}/keoz/keoz-checklist", name="api.action.keoz.keoz-checklist", methods={"GET"})
     */
    public function index(Request $request, Context $context)
    {
        $result = [];

        foreach ($this->services as $checkService) {
            $result = array_merge($result, $checkService->getCheckResult());
        }

        return new JsonResponse([
            'data' => $result,
        ]);
    }
}
